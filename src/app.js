class Cube {
    constructor(length) {
        this.length = length;
    }
    
    getSideLength () {
        return this.length;
    }
    
    getSurfaceArea () {
        return (this.length * this.length) * 6;
    }
    
    getVolume () {
        return Math.pow(this.length,3);
    }
}


class Triangle {
    constructor(length1,length2,length3) {
        this.length1 = length1;
        this.length2 = length2;
        this.length3 = length3;
    }
    
    getLength1 () {
        return this.length1;
    }
    
    getLength2 () {
        return this.length2;
    }
    
    getLength3 () {
        return this.length3;
    }
    
    getSurfaceArea () {
        var s = 0;
        var Air = 0;
        s = (this.length1 + this.length2 + this.length3)/2
        Air = Math.sqrt(s*((s-this.length1)*(s-this.length2)*(s-this.length3)))
        return Math.round(Air * 100)/100 //pour arrondir à 2 décimales
    }

    getHypotenuse() {
        return Math.max(this.length1,this.length2,this.length3)
    }

    isEquilateral() {
        if (this.length1==this.length2){
            if (this.length1==this.length3){
                return true
            }
        }
        return false
    }
    
}

class Sphere {
    constructor(diameter) {
        this.diameter = diameter;
    }
    
    getDiameter () {
        return this.diameter;
    }

    getRadius () {
        return this.diameter/2;
    }
    
    getSurfaceArea () {
        var pi = 3.1415
        var surfaceArea = pi*Math.pow(this.diameter/2,2)
        return Math.round(surfaceArea * 100)/100 //pour arrondir à 2 décimales 
    }
    
    getVolume () {
        var pi = 3.1415
        var volume = (4*pi*Math.pow(this.diameter/2,3))/3
        return Math.round(volume * 100)/100 //pour arrondir à 2 décimales 
    }
}

module.exports = {
    Cube:Cube,
    Triangle:Triangle,
    Sphere:Sphere
}
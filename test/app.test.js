const Cube = require('../src/app').Cube;
const Triangle = require('../src/app').Triangle;
const Sphere = require('../src/app').Sphere;
const expect = require('chai').expect;

describe('Testing the Cube Functions', function() {
    it('1. The side length of the Cube', function(done) {
        let c1 = new Cube(2);
        expect(c1.getSideLength()).to.equal(2);
        done();
    });
    
    it('2. The surface area of the Cube', function(done) {
        let c2 = new Cube(5);
        expect(c2.getSurfaceArea()).to.equal(150);
        done();
    });
    
    it('3. The volume of the Cube', function(done) {
        let c3 = new Cube(7);
        expect(c3.getVolume()).to.equal(343);
        done();
    });
    
});

describe('Testing the Triangle Functions', function() {
    it('1. The 3 sides length of the Triangle', function(done) {
        let t1 = new Triangle(2,3,4);
        expect(t1.getLength1()).to.equal(2);
        expect(t1.getLength2()).to.equal(3);
        expect(t1.getLength3()).to.equal(4);
        done();
    });
    
    it('2. The surface area of the Triangle', function(done) {
        let t2 = new Triangle(10,7,8);
        expect(t2.getSurfaceArea()).to.equal(27.81);
        done();
    });
    
    it('3. The Hypotenuse of the Triangle', function(done) {
        let t3 = new Triangle(10,7,8);
        expect(t3.getHypotenuse()).to.equal(10);
        done();
    });
    
    it('4. Is the Triangle equilateral', function(done) {
        let t4 = new Triangle(7,7,8);
        let t4bis = new Triangle(7,7,7);
        expect(t4.isEquilateral()).to.equal(false);
        expect(t4bis.isEquilateral()).to.equal(true);
        done();
    });

});

describe('Testing the Sphere Functions', function() {
    it('1. The Diameter and radius of the circle', function(done) {
        let s1 = new Sphere(6);
        expect(s1.getDiameter()).to.equal(6);
        expect(s1.getRadius()).to.equal(3);
        done();
    });
    
    it('2. The surface area of the cicrle', function(done) {
        let s2 = new Sphere(10);
        expect(s2.getSurfaceArea()).to.equal(78.54);
        done();
    });
    
    it('3. The volume of the Sphere', function(done) {
        let s3 = new Sphere(7);
        expect(s3.getVolume()).to.equal(179.59);
        done();
    });
    
});